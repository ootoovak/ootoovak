---
title: Talks
date: 2016-07-04 13:50:58
---

Below are the talks that I have given so far. Feel free to contact me if you would like me to give a talk at your event. I have been programming professionally for many years and I am very interested in how we can get better at communicating our ideas when build software both at a beginner and experienced level. I am also interested in talking about how we can bring more empathy into what we do in the tech industry.

- [Continual Code Review](http://continual-code-review.surge.sh/)
  - Given at the Code Camp Wellington 2016 conference.
  - A talk about pair programming, advantages, pitfalls, and tips.
- [Micro App Deploys](https://speakerdeck.com/ootoovak/micro-app-deploys)
  - Given at WellRailed the Wellington Ruby user group.
  - A talk about Dokku and my experiences using it to deploy to a VPS.
- [Lightning Talk on the Virtus Gem](https://speakerdeck.com/ootoovak/virtus-lightning-talk)
  - Given at WellRailed the Wellington Ruby user group
  - Talking about using Virtus for things like Form Objects.
- [Programming for People](https://speakerdeck.com/ootoovak/programming-for-people)
  - Given at the first RailsGirls Wellington
  - Brief history of programming and why it is important to keep people in mind when you program.

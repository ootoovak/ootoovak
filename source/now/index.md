---
title: Now
date: 2016-07-04 20:29:12
updated: 2018-07-01 21:55:23
---

What I am working on now is this site, keeping it up to date and shifting it to more of a personal space. Also, I have been moving my programming project stuff to {% link "Forefront Projects" https://forefrontprojects.com %}. Forefront Projects is the place I want to help me focus, progress and share my project work.

Computer things I am interested in the moment are.

These programming languages (in current level of interest order):
- {% link "Rust" https://www.rust-lang.org/ %}
- {% link "Elm" http://elm-lang.org/ %}
- {% link "Elixir" http://elixir-lang.org/ %}

I am also interested in distributed systems so I am interested in looking into how the following tech might work into a distributed AI system:
- {% link "MAIDSafe" https://maidsafe.net/ %}
- {% link "Blockstack" https://blockstack.org/ %}
- {% link "Dat" https://datproject.org/ %}

Finally, I have bought some prototype hardware which I will aim to incorporate into projects:
- {% link "Tessel 2" https://tessel.io/ %}
- {% link "Particle Photon" https://www.particle.io/ %}

Non-computer things I am interested in are going to shows, enjoying relaxing time at home and hanging out with awesome friends in wonderful Wellington, NZ. :)

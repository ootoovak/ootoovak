---
no_date: true
---

![Samson Ootoovak (me)](/me.jpg)

## Hello!

I am Samson Ootoovak and this is my personal site. I like computers AND people, think about programming as communication, and like inclusive spaces. I am a Indigenous Canadian (Inuk) and a New Zealand immigrant. I have some writing [here](/archives) and if you are interested in what I am up to you can check out my [now](/now) page and my projects site called [Forefront Projects](https://forefrontprojects.com).
---
title: Contact
date: 2016-07-04 23:05:58
---

If you would like to reach me, this is me across the internet:

- {% link "Ootoovak on Twitter" http://twitter.com/ootoovak %}
- {% link "Ootoovak on Gitlab" http://gitlab.com/ootoovak %}
- {% link "Ootoovak on LinkedIn" http://nz.linkedin.com/in/ootoovak %}

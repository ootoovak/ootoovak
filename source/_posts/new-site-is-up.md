---
title: New site is up!
date: 2016-07-05 14:03:57
tags:
  - blog
  - personal
---

Just a official announcement (if you haven't seen it already by the different styling of the site) that I have got my new blog set up. I have been chipping away at it for a while moving from the [Harp](https://harpjs.com/) static site generator to [Hexo](https://hexo.io/). It is also a static site generator but with more of a focus on blogs as well as more support in terms of eyes on the code, plugins and themes available.

So yes, I did fall in to that typical developer trap of building my own blog and theme and it wasn't a particularly fun process but glad I did it. I like having and owning my own written content, having it in plain text that I can edit anywhere. I also like the flexibility I have to modify my blog the way I see fit now that the base has been set up.

Another thing I did was that I realised that right now I don't need a VPS to host this blog or any JS examples I had. I was using [Digital Ocean](https://www.digitalocean.com/) for a while, and they were great, but for now all I need is a static site host, and so when [Gitlab introduced hosted pages](https://pages.gitlab.io/) it seemed like a good fit (I did consider [Surge](http://surge.sh/) prior to that).

Besides the above, one other reason I was wanting to rework my "blog" was because I wanted a bit more than just a linear blog. I was feeling that I couldn't post "unfinished" or partial updates to projects as they would either lack context or get lost as I interleaved updates from other projects and as I jumped between them. As a result of the move I will soon be creating a project page to list links to project updates, and as a nice side affect I feel like it also gives me room to share personal, non-tech posts if I feel like writing them.

In any case it is all up now, and I'm looking forward to digging into some new projects and sharing the updates soon!

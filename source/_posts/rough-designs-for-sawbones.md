---
title: Rough Designs for Sawbones
date: 2014/1/10
updated: 2014/1/10
tags:
  - project
  - sawbones
---

I am going to try this thing where I document as much as I can during this process of building an open source application. While some of it might be a bit embarrassing to look back on someday, I think all the more reason to post it now.

Below are two of the designs I have sketched out for Sawbones so far. The only design page I drew but haven't included in this post was one of the fields needed for the initial setup screen, the reason being that I had a bunch of other stuff around it that made it confusing and obfuscated.

For documentation sake the page had the following information:

## Basic ideas on un-photographed page

In the first build out I am going to assume a lot. This is so the project can work for me as soon as possible and thus be useful and tested though use. From there the plan is to add generalisations that do not force too much complexity into the app setup.

### Assumptions

* Using Rails 3+
* Using PostgreSQL
* Using Git
* Using Github
* A version of the app under test has been setup (or proven) to run on server running Sawbones
* Will have some sort of pathname convention as to were project files are stored

### Fields on initial setup page

1. Branches
2. Run Commands
3. Notification Emails

## The other two pages

{% asset_img rough-designs-1.jpg Sawbones Rough Design 1 %}
{% asset_img rough-designs-2.jpg Sawbones Rough Design 2 %}

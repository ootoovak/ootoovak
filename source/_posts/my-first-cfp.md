---
title: My First CFP
date: 2015/11/23
updated: 2016/5/2
tags:
  - cfp
  - programming
---

*Recently I submitted a proposal to a conference and while it didn't get accepted I wanted to share my proposal anyway, both as a reference to anyone that might be interested in hearing me talk about it but also as an example of a first timer's CFP (Call For Proposal). Shout-out to [Cara Hill](https://ilikeprettycode.wordpress.com/) for reviewing the proposal and giving content and grammar feedback.*

Are Our Tools Broken? Mental Building Blocks and Metaphors in Programming
-------------------------------------------------------------------------

### Bio
Samson has been found around computers since he was young but did not start programming in earnest until his mid-twenties when he discovered he was reading programming books in engineering classes. Since completing a Computer Science degree, Samson has spent the last 8 years working with many different companies as an employee, consultant and teacher. He strongly believes that people should always be the focus of technology and likes thinking about programming as a form of communication.

### Abstract
Programming is difficult and most of the tools we use to program are not helping. From learning to teaching, we use and share mental models and metaphors as hand holds to make the heavy lifting a bit easier. So what are these metaphors? How do they differ between people/groups? And even more interesting, which ones might be holding us back? Join me in an examination of the mental building blocks we play with every day.

### Details
This talk will focus on the communication devices used in programming; I will discuss the frequently used metaphors that we might not even think about, including those that gave rise to object-oriented programing and modern GUIs, and some of the less explored mental and visual metaphors. I will propose some new ideas I have been contemplating in this space (using more than just coloured text!). I will also discuss how our tools might be designed to help with reasoning about our computer systems, especially as they grow in complexity and are maintained by more and more people.

The target audience will be wide, from brand new programmers to seasoned developers. The talk will not only aim to provoke thought on how we communicate our ideas to each other, but also explore what kind of thinking our tools might be promoting with or without our direct knowledge or understanding. I would like the audience to come away from the talk thinking about programming in a way they may not have before. Whether that is thinking about code like prose or about how more of our senses could be engaged to hold more information in our heads as we develop new programs.

### Pitch
I think this subject is pertinent given that developers are asked to understand a constant flow of information in order to program effectively. We need to know how to work with concepts like object-oriented and functional programming, static pages and live streams of data, large legacy applications and many small systems talking together. I think it is important to stop to reflect on how we communicate that information, not only to become better programmers but also to appreciate how young our profession really is and how many ideas are yet to be explored.

I have a Computer Science degree, have spent over 7 years working in the web development industry as both a contractor and employee, and have more recently taught programming in a web development bootcamp-style course on Ruby, Javascript and other web technologies. Having done all of this I feel like I have been exposed to many different learning, conceptualisation and communication styles and as a result have rediscovered and refined my own style of understanding and modelling our virtual world, many times over.

Notes & Feedback
----------------

### Feedback from [Carina C. Zona](https://twitter.com/cczona)

- Include the takeaways in the abstract too. It's going to be what the audience reads, and they will make decisions based on how much those takeaways seem valuable for them.
- The "details" and "pitch" sections are atypical. Are these private comments fields to the program committee?
- Eliminate hedge language. "Might", etc. Recast it in declarative language. A speaker is positioning themselves as an authority on what they're talking about. Not necessarily the only authority. But one.
- (This applies to "I think", etc, too.)
- The takeaways seem to all end at stirring thought/curiosity/etc. Can you take it a step further into action takeaways that they'll get from the thinking?
- Extend the declarative to the title too. Program committees and audiences are intrigued by a title and opening sentence that are a bit of controversial throwdown. It's compelling, as long as the rest of the abstract evidences there's credible case to make for it.
- e.g. "Our Tools Are Broken." Bam.

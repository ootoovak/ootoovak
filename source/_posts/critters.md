---
title: Critters
date: 2015/2/15
updated: 2015/2/15
tags:
  - critters
  - programming
  - simulations
---

So [the last time I had some time off](/2014/10/09/my-ever-changing-plan-to-produce) I finally got around to programming something I have been talking about for years, multiagent systems simulations! More specifically, heterogeneous multiagent systems, and what finally got me working on this this long pined for goal was the fantastic tutorial by Jarrod Overson called [Build a Javascript Particle System In 200 Lines](http://html5hub.com/build-a-javascript-particle-system/) that was listed in [JavaScript Weekly](http://javascriptweekly.com/). It gave me a a quick and easy path to creating and visualising the simulation which I feel is a big (and exciting) part of the process.

Before starting I had some rough goals in mind for the project. I wanted to practice and improve my JavaScript (JS) programming, get familiar with the CommonJS module system, learn how to do animations on Canvas and of course play around with autonomous agents.

As mentioned one of the reasons the tutorial by Jarrod excited me so much was because it gave me an easy in to fine grained control of drawing and animating in a space I was already quite comfortable in, JS and web technologies. (As an aside, I think this is one of the great things about [NodeBots](http://nodebots.io/)!) I have been doing animation with DOM elements for quite some time now (it is part of my job after all) but using Canvas just opens up a whole new space and for some reason to me it *feels* different; perhaps it feels like there is more controll without having to worry about HTML and CSS as well as JS. Another thing that I enjoyed about the tutorial was that it was not related to a library or framework. I have spent a lot of time, perhaps too much time, over the years up-skilling on various web related libraries or frameworks and while they are usually an overall positive, there is something to be said for just digging in with the languages itself as much as possible. Something I should try to remember more often.

So, after coming across the tutorial the first step was just to follow the tutorial exactly as laid trying to understand how to interact with canvas to draw things as well as implementing and interacting with the animation loop. Given my previous knowledge of Javascript and the fact the tutorial was very detailed that part didn't take too long at all.

{% asset_img emit-and-repulse.png Basic Tutorial Example %}

As someone who is sometimes autodidactic to a fault (more on that another time) I have followed along with a lot of tutorials. One technique I have found that has helped me has been redoing them and following the techniques but changing just a few things slightly. So once I got to the end of the original tutorial I just scrapped the code and started writing it again this time with Object Oriented JavaScript (OOJS) in mind.

I progressed along this path creating objects and eventually getting my code to create something similar to the tutorial but written with JS objects rather than procedurally. As mentioned in the original tutorial this may have slowed my simulation down but my goal was also to write maintainable code as well. During this rewrite I was feeling pretty comfortable with the concepts and I wanted to keep things fun so I also played around with the number and visualisation of the particle emitters and world elements and had quite a fun time doing so.

{% asset_img example.jpg Particles %}

Once I had got my code to that point where I had got a better feel for HTML Canvas and how and animation run loop worked I once again pulled mostly everything out and started again (aren't side projects great!). The goal this time around was to move to an MVC pattern, even though I wasn't sure it was going to be the best pattern for this project, as well as starting to build up the idea of a world ready for beings to inhabit.

With that in mind I started to slowly build up the view layer, just getting anything to show up, playing around with shapes, colours and movement. Also, because I am awesome, I chose to start off with multicoloured shimmering circles. Seeing a whole bunch of multicoloured semitransparent dots moving around on the screen all at once made me strangely happy and content (likely some Christmas tree lights memories being triggered). Speaking of happy I was also glad that I was able to the methods [sparkle and shine](https://gitlab.com/ootoovak/critters/commit/074dfa9428b27adf56b0aca7a26fe8d915ac004a#421ca93d04488cbd137acaebdfe2a3a0ae28687d_30_43) to my view code.

{% asset_img random-dots.png Random Dots %}
{% asset_img flashing-lights.png Flashing Lights %}

So now that I had a view layer working and drawing things where and how I wanted it was time to move on to modeling the world and the agents (what I decided to call *Beings*) in it. So as my original goal of the simulation was to have a heterogeneous system I wanted the world to be able to start off being able to handle multiple types of Beings. Even so I started them all off with the same behaviour. After a bit of working that got me to fly like swarms.

{% asset_img swarming.png Simple Swarming %}

Once I got all the Beings them swarming the same rules the next goal was to give them each unique behaviour based on their type and the other Beings around them. This was one of the funnest parts to of the project, playing around with the simple rules, wiring up the models to the view and seeing it in action, was just so awesome! There is a book by Michael Crichton called [Prey](http://bit.ly/1vOuHdM) and it was one of the things that got me interested in artificial life in ernest and one of the reasons I took Computer Science in university, so to finally be seeing something I wrote look and act like a proper swarm was pretty exiting!

{% youtube nhzO9qE67pY %}

At the moment the rules are not fully where I want them to be. For example the green "Plants" just just randomly wiggle at the moment. There is also no working concept of eating yet so even when the red "Predators" catch the yellow "Prey" or the prey catches reaches the plants nothing happens. But it is a start with real complex behaviour emerging.

This project really came out of wanting to keep my fingers moving for a change rather than getting stuck in to reading things as I do so often. What I found was that visuals, particularly animation, really get me excited and motivated. Being able to see my code in live in action is pretty amazing, especially when it produces unexpected behaviour (on purpose this time!). Moving forward my plan is to keep using this as a platform for experimentation both in JS programming and visualising emergent behaviour but mostly I just had a lot of fun and that will continue to be my driving force [with this project](https://gitlab.com/ootoovak/critters). :)

For a glimpse at the current running state of the project please feel free to check out http://critters.forefrontprojects.com.

Note: I also want to give a shout out to [Mikey Williams](http://dinosaur.is) for helping me to get going with NPM and Browserify on this project.

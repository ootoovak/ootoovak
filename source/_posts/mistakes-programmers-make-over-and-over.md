---
title: Mistakes Programmers Make Over and Over
date: 2015/7/26
updated: 2015/7/26
tags:
  - reflection
  - programming
---

So, over the last year I have been a teacher at [Enspiral Dev
Academy](<http://devacademy.co.nz>), teaching people how to program. During this
time I have found a lot of what I teach comes directly from my own learnings,
which are often based off of mistakes and patterns I have seen during my years
as a professional web developer.

While teaching the course, it is amazing to see how much people can learn in
such a short period of time, each person's individual learning style and how
they model the concepts of programming in their heads. One thing I have noticed
is that the common concepts I have to repeat most often to students are also the
same things that I still do over and over again.

So, the following are some mistakes that we as programmers seem to be doomed to
repeat, even when we should know better and teach people not to do them:

Read the errors
---------------

>   "Read the errors, read the errors, read the errors."

Most errors give you a lot of information. Good errors not only point you
directly to the file and line number that is causing the issue, but also give a
name as to what has gone wrong. So, what is the first thing we do as programmers
as soon as we see an error? We switch back to the code without reading the
error.

We have just been in the code writing it so we instinctively assume we know what
the error is and we go off to fix it. More often than not though we are off
trying to fix the wrong thing, or we start reading the code and realise that we
actually have no idea what went wrong and we inevitably have to go back and read
the error that was giving us the feedback in the first place.

Save the middle step and read the errors first.

Copy-paste trouble
------------------

>   "Copying and pasting code will always get you into trouble."

It is always tempting to copy code. The more code there is, the more tempting it
is and the more likely it is to get you in trouble if you have to modify any
part of it.

Maybe it is an example that you are learning from, maybe it is some code you
have written in another project and you just want to copy over that snippet.
That corner cutting more often than not leads to forgetting to update something
then having to read through all of the code and hunt down the error. Not to
mention you miss the learning involved with going through the code step-by-step
either for the first time or again.

Sometimes the copy-paste works out ok, but more often then not you spend just as
much time (if not more) hunting down that variable name you forgot to change.

Keeping your branches small
---------------------------

>   "It is really easy to either get off track working in a branch or try to do
>   too much. Keep your branches small."

This is one of my favourites. And it happens in two flavours, “the massive
little clean up” and "the asymptote of perfection". In either case it ends up
the same way. Weeks of work and a massive diff that no one could ever hope to
code review effectively.

In the "massive little clean up" you start off work on a branch and in the
middle you run into a personal bugaboo. It can be something as insidious as
spacing. In the best case, the file you are working in gets tonnes of little
diffs distracting from the original fix. In the worst case, you end up adding
and commiting 25 files with a commit message along the lines of “changed a bunch
of stuff”.

In "the asymptote of perfection" you start a brand new branch, all happy, about
to fix something or add something new. Three weeks later you're downtrodden,
your branch forever on the verge of "almost being done" and while you want to
finish the branch and have it available for review you feel bad for the poor
soul that has to make sense of the pages worth of diffs. Even so, you just keep
finding things to make better, chasing after perfect code.

In either case, the disipline is similar to that of small classes or methods.
Planning for small changes and determining if they are the responsibility of the
current branch.

Quickly not writing tests
-------------------------

>   "I am just going to write this quickly without tests."

It starts out innocently enough, as it always does. You go in thinking it is
going to be an easy change and will be much quicker just to bang out the code
quickly and write test coverage for it later. But then your changes start
growing, spanning more than one object, or the code flow is just a bit more
complex then you anticipated. But you have already committed, so you soldier on,
attempting to keep the code flow you are currently tracing all in your head
while writing even more code.

This one happens to newbies because on the surface it makes sence. Writing less
code in one place is quicker and easier than having to think about writing more
code in two places. But after getting burned for not having tests a few times,
we realise it is a false sense of speed and we start writing tests at the start.
Over time, we become more confident programmers thinking we can hold more code
in our head at once [and the cycle happens all over
again.](<https://twitter.com/ootoovak/status/602724360802082816>)

Avoid the allure of “just a quick fix”, give your brain a break, write tests
from the start.

Starting is better than perfect
-------------------------------

>   "It is tempting to want to do it perfectly the first time but starting is
>   better than perfect."

You are looking at using a new library, framework, language or design pattern.
So what do you do? You read about it. At first the docs, then maybe a tutorial
or two. Then maybe the API docs and perhaps something detractors have written.
Anything to keep yourself from, you know, actually starting. Because if you do
start writing code then there is a possibility you might start it wrong.

I noticed this one in students I was teaching, coaching them to just experiment
and try things out without worrying about getting it perfect in the first go. I
empathised with them because I do it myself. Now I am more consciously aware of
it when it happens to me. I noticed the strong pull to do anything but start
cutting code because starting could mean not doing it perfectly the first time,
and perfection is just not an attainable goal. It is a lesson that is often
easier to teach others than yourself but it is a good one to try to remember.

Do read and think about your problem but don’t let it become an artificial
blocker. You can always improve (or even delete) code and the process of
starting will teach you more about the problem than reading about other people's
attempts.

Everyone is better than me
--------------------------

>   "Everyone else knows more or is doing more than me."

This is a tough one. I think in any industry or social situation it is difficult
not to compare yourself to others but in a such a young, fast moving industry
like ours it sometime feels even worse. Not only are we likely to see other
people in a much more favourable light then ourselves, but also we usually see
them presented at their best. Whether that is online, in a classroom, at a user
group, or at a conference.

Seeing and hearing people do amazing things is inspiring but can also leave you
feeling wanting, [like you are not living up to some make believe
bar](<https://twitter.com/ootoovak/status/574781277691248640>). Finding people
you admire is great, take inspiration and then use that to focus on your own
path. You can draw inspiration from others but you will never be traveling the
exact same path as them and that is ok, it does not make them better and it does
not make you worse. You as an individual bring a different experience to the
table and that is a very valuable thing.

Try and practice changing your view from “everyone is better” to “everyone is
different”.

So there you have it
--------------------

If you are starting out as a programmer then these are some things I would
advise you keep an eye out for. If you have been programming for years then
these are some things I would advise you to keep an eye out for. I might have to
bookmark this post myself every time I sit down to start a new project.

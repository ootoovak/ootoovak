---
title: The Personal and the Project
date: 2018-02-02 15:42:19
tags:
    - personal
    - personalise
    - project
---

So, I have had the domain [forefrontprojects.com](https://forefrontprojects.com) for a while now and I haven't really done anything with it. The intention was always to have it be a place where I showcased projects I was working on. Recently, I got my act together and started working on it in ernest ([with sidetracks galore obviously](https://twitter.com/ootoovak/status/932788724542271488)). But it is getting there, and as I was building the site I started thinking about not only what I wanted [Forefront Projects](https://forefrontprojects.com) to be but also what that meant for this domain and website. I have always felt a little guilty using this domain for just me as I have taken up the whole family namespace. While there are not that many Ootoovaks in the world (even fewer that spell it the same way) there are some. So while I'm not going so far as to start using a subdomain for now I thought I should keep the core of my project work on a different domain so it is less likely to need URL changes in the future.

I am still going to write here. I'm going to use this as both as a place to post personal stuff as well as a place to write about larger, more conceptual programming and technology things that don't quite fit into the projects I happen to be working on at the time.

In any case, that's enough from me for now, I'm excited to start getting into the project stuff and start sharing the progress over at [Forefront Projects](https://forefrontprojects.com).
---
title: First Steps
date: 2013/12/29
updated: 2013/12/29
tags:
  - personal
---

Creating a blog that I will actually use is the first step toward working on projects that have had in the back of my mind for a while and I have kept on the back burner for far too long. While I likely spent way too much time creating a blog (as programmers do) I do feel that it is exactly what I need now and I can grow it in exactly the way I want.

While the above was the driving force for starting and actually completing this blog the contents are also likely to include my general thoughts on programming as well. My day job is that of a web developer and so invariably there will be posts on the tools I use day to day too.

Up next, {% post_link introduction-to-sawbones Sawbones %}.

# Samson Ootoovak's Blog

## Software

This blog is built with [Hexo](https://hexo.io/).

### Running development server

`hexo server`

### Writing a new post

`hexo new <post-title>`
